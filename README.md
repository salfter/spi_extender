SPI Extender
============

Park one of these between a TMC2130 or TMC5160 stepper driver and a
3D-printer control board to bring out the SPI bus used to configure the
driver for other purposes (like a thermocouple or RTD digital converter). 
Use any other free digital I/O pin as CS for your add-on SPI peripheral.

The "horizontal" project is laid out to work with the SKR 1.4 Turbo and
similar boards.  The "vertical" project is laid out to work with the SKR Pro
boards, where the drivers are turned 90° and need a different layout.

No BOM provided; all you need are 18 pins' worth of sockets that will hold
the driver boards' pins on one side and long pins on the other side that
will extend through the board and into the socket on your printer's control
board.  Something like the Samtec SSQ-10x-03-G-S (two with 8 pins, one with
2) would probably work.



