EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x05 J1
U 1 1 6088C71C
P 4650 4000
F 0 "J1" H 4568 3575 50  0000 C CNN
F 1 "Conn_01x05" H 4568 3666 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 4650 4000 50  0001 C CNN
F 3 "~" H 4650 4000 50  0001 C CNN
	1    4650 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	4850 3900 5250 3900
Wire Wire Line
	4850 4000 5250 4000
Wire Wire Line
	5250 4300 5100 4300
Wire Wire Line
	5100 4300 5100 4100
Wire Wire Line
	5100 4100 4850 4100
Wire Wire Line
	6850 4300 7050 4300
Wire Wire Line
	7050 4300 7050 4500
Wire Wire Line
	7050 4500 5000 4500
Wire Wire Line
	5000 4500 5000 4200
Wire Wire Line
	5000 4200 4850 4200
Wire Wire Line
	4850 3800 5000 3800
Wire Wire Line
	5000 3800 5000 3400
Wire Wire Line
	5000 3400 5250 3400
$Comp
L pololu:POLOLU18 U1
U 1 1 60891C89
P 6050 3900
F 0 "U1" H 6050 4753 60  0000 C CNN
F 1 "POLOLU18" H 6050 4647 60  0000 C CNN
F 2 "pololu:pololu18" H 6050 4541 60  0000 C CNN
F 3 "" H 6050 3900 60  0000 C CNN
	1    6050 3900
	1    0    0    -1  
$EndComp
$EndSCHEMATC
